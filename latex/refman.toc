\contentsline {chapter}{\numberline {1}Objetivo}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Ball Class Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{12}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Constructor \& Destructor Documentation}{12}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}Ball()}{12}{subsubsection.5.1.2.1}
\contentsline {subsection}{\numberline {5.1.3}Member Function Documentation}{12}{subsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.3.1}display()}{12}{subsubsection.5.1.3.1}
\contentsline {subsubsection}{\numberline {5.1.3.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X()}{12}{subsubsection.5.1.3.2}
\contentsline {subsubsection}{\numberline {5.1.3.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Y()}{12}{subsubsection.5.1.3.3}
\contentsline {subsubsection}{\numberline {5.1.3.4}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}X()}{12}{subsubsection.5.1.3.4}
\contentsline {subsubsection}{\numberline {5.1.3.5}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Y()}{13}{subsubsection.5.1.3.5}
\contentsline {subsubsection}{\numberline {5.1.3.6}step()}{13}{subsubsection.5.1.3.6}
\contentsline {subsection}{\numberline {5.1.4}Member Data Documentation}{13}{subsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.4.1}g}{13}{subsubsection.5.1.4.1}
\contentsline {subsubsection}{\numberline {5.1.4.2}m}{13}{subsubsection.5.1.4.2}
\contentsline {subsubsection}{\numberline {5.1.4.3}r}{13}{subsubsection.5.1.4.3}
\contentsline {subsubsection}{\numberline {5.1.4.4}vx}{13}{subsubsection.5.1.4.4}
\contentsline {subsubsection}{\numberline {5.1.4.5}vy}{13}{subsubsection.5.1.4.5}
\contentsline {subsubsection}{\numberline {5.1.4.6}x}{13}{subsubsection.5.1.4.6}
\contentsline {subsubsection}{\numberline {5.1.4.7}xmax}{13}{subsubsection.5.1.4.7}
\contentsline {subsubsection}{\numberline {5.1.4.8}xmin}{14}{subsubsection.5.1.4.8}
\contentsline {subsubsection}{\numberline {5.1.4.9}y}{14}{subsubsection.5.1.4.9}
\contentsline {subsubsection}{\numberline {5.1.4.10}ymax}{14}{subsubsection.5.1.4.10}
\contentsline {subsubsection}{\numberline {5.1.4.11}ymin}{14}{subsubsection.5.1.4.11}
\contentsline {section}{\numberline {5.2}Ball\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Drawable Class Reference}{15}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{17}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Constructor \& Destructor Documentation}{17}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}Ball\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Drawable()}{17}{subsubsection.5.2.2.1}
\contentsline {subsection}{\numberline {5.2.3}Member Function Documentation}{17}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}display()}{17}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}draw()}{17}{subsubsection.5.2.3.2}
\contentsline {section}{\numberline {5.3}Drawable Class Reference}{17}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Member Function Documentation}{18}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}draw()}{18}{subsubsection.5.3.1.1}
\contentsline {section}{\numberline {5.4}Figure Class Reference}{19}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Constructor \& Destructor Documentation}{20}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Figure()}{20}{subsubsection.5.4.1.1}
\contentsline {subsubsection}{\numberline {5.4.1.2}$\sim $\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Figure()}{21}{subsubsection.5.4.1.2}
\contentsline {subsection}{\numberline {5.4.2}Member Function Documentation}{21}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}add\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Drawable()}{21}{subsubsection.5.4.2.1}
\contentsline {subsubsection}{\numberline {5.4.2.2}draw()}{21}{subsubsection.5.4.2.2}
\contentsline {subsubsection}{\numberline {5.4.2.3}draw\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Circle()}{21}{subsubsection.5.4.2.3}
\contentsline {subsubsection}{\numberline {5.4.2.4}draw\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Line()}{21}{subsubsection.5.4.2.4}
\contentsline {subsubsection}{\numberline {5.4.2.5}draw\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String()}{21}{subsubsection.5.4.2.5}
\contentsline {subsubsection}{\numberline {5.4.2.6}remove\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Drawables()}{21}{subsubsection.5.4.2.6}
\contentsline {subsubsection}{\numberline {5.4.2.7}reshape()}{22}{subsubsection.5.4.2.7}
\contentsline {subsubsection}{\numberline {5.4.2.8}update()}{22}{subsubsection.5.4.2.8}
\contentsline {section}{\numberline {5.5}Mass Class Reference}{22}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Constructor \& Destructor Documentation}{24}{subsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.1.1}Mass()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{24}{subsubsection.5.5.1.1}
\contentsline {subsubsection}{\numberline {5.5.1.2}Mass()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{24}{subsubsection.5.5.1.2}
\contentsline {subsection}{\numberline {5.5.2}Member Function Documentation}{24}{subsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.2.1}add\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Force()}{24}{subsubsection.5.5.2.1}
\contentsline {subsubsection}{\numberline {5.5.2.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Energy()}{24}{subsubsection.5.5.2.2}
\contentsline {subsubsection}{\numberline {5.5.2.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Force()}{25}{subsubsection.5.5.2.3}
\contentsline {subsubsection}{\numberline {5.5.2.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass()}{25}{subsubsection.5.5.2.4}
\contentsline {subsubsection}{\numberline {5.5.2.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Position()}{25}{subsubsection.5.5.2.5}
\contentsline {subsubsection}{\numberline {5.5.2.6}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Radius()}{25}{subsubsection.5.5.2.6}
\contentsline {subsubsection}{\numberline {5.5.2.7}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Velocity()}{25}{subsubsection.5.5.2.7}
\contentsline {subsubsection}{\numberline {5.5.2.8}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Force()}{25}{subsubsection.5.5.2.8}
\contentsline {subsubsection}{\numberline {5.5.2.9}step()}{25}{subsubsection.5.5.2.9}
\contentsline {subsection}{\numberline {5.5.3}Member Data Documentation}{25}{subsection.5.5.3}
\contentsline {subsubsection}{\numberline {5.5.3.1}force}{25}{subsubsection.5.5.3.1}
\contentsline {subsubsection}{\numberline {5.5.3.2}mass}{25}{subsubsection.5.5.3.2}
\contentsline {subsubsection}{\numberline {5.5.3.3}position}{26}{subsubsection.5.5.3.3}
\contentsline {subsubsection}{\numberline {5.5.3.4}radius}{26}{subsubsection.5.5.3.4}
\contentsline {subsubsection}{\numberline {5.5.3.5}velocity}{26}{subsubsection.5.5.3.5}
\contentsline {subsubsection}{\numberline {5.5.3.6}xmax}{26}{subsubsection.5.5.3.6}
\contentsline {subsubsection}{\numberline {5.5.3.7}xmin}{26}{subsubsection.5.5.3.7}
\contentsline {subsubsection}{\numberline {5.5.3.8}ymax}{26}{subsubsection.5.5.3.8}
\contentsline {subsubsection}{\numberline {5.5.3.9}ymin}{26}{subsubsection.5.5.3.9}
\contentsline {section}{\numberline {5.6}Simulation Class Reference}{27}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Detailed Description}{28}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Member Function Documentation}{28}{subsection.5.6.2}
\contentsline {subsubsection}{\numberline {5.6.2.1}display()}{28}{subsubsection.5.6.2.1}
\contentsline {subsubsection}{\numberline {5.6.2.2}step()}{28}{subsubsection.5.6.2.2}
\contentsline {section}{\numberline {5.7}Spring Class Reference}{29}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Constructor \& Destructor Documentation}{29}{subsection.5.7.1}
\contentsline {subsubsection}{\numberline {5.7.1.1}Spring()}{29}{subsubsection.5.7.1.1}
\contentsline {subsection}{\numberline {5.7.2}Member Function Documentation}{30}{subsection.5.7.2}
\contentsline {subsubsection}{\numberline {5.7.2.1}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Energy()}{30}{subsubsection.5.7.2.1}
\contentsline {subsubsection}{\numberline {5.7.2.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Force()}{30}{subsubsection.5.7.2.2}
\contentsline {subsubsection}{\numberline {5.7.2.3}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Length()}{30}{subsubsection.5.7.2.3}
\contentsline {subsubsection}{\numberline {5.7.2.4}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass1()}{30}{subsubsection.5.7.2.4}
\contentsline {subsubsection}{\numberline {5.7.2.5}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass2()}{30}{subsubsection.5.7.2.5}
\contentsline {subsection}{\numberline {5.7.3}Member Data Documentation}{30}{subsection.5.7.3}
\contentsline {subsubsection}{\numberline {5.7.3.1}damping}{30}{subsubsection.5.7.3.1}
\contentsline {subsubsection}{\numberline {5.7.3.2}mass1}{30}{subsubsection.5.7.3.2}
\contentsline {subsubsection}{\numberline {5.7.3.3}mass2}{30}{subsubsection.5.7.3.3}
\contentsline {subsubsection}{\numberline {5.7.3.4}natural\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Length}{30}{subsubsection.5.7.3.4}
\contentsline {subsubsection}{\numberline {5.7.3.5}stiff}{31}{subsubsection.5.7.3.5}
\contentsline {subsubsection}{\numberline {5.7.3.6}stiffness}{31}{subsubsection.5.7.3.6}
\contentsline {section}{\numberline {5.8}Spring\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass Class Reference}{31}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}Constructor \& Destructor Documentation}{32}{subsection.5.8.1}
\contentsline {subsubsection}{\numberline {5.8.1.1}Spring\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass()}{32}{subsubsection.5.8.1.1}
\contentsline {subsection}{\numberline {5.8.2}Member Function Documentation}{32}{subsection.5.8.2}
\contentsline {subsubsection}{\numberline {5.8.2.1}display()}{32}{subsubsection.5.8.2.1}
\contentsline {subsubsection}{\numberline {5.8.2.2}get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Energy()}{33}{subsubsection.5.8.2.2}
\contentsline {subsubsection}{\numberline {5.8.2.3}step()}{33}{subsubsection.5.8.2.3}
\contentsline {subsection}{\numberline {5.8.3}Member Data Documentation}{33}{subsection.5.8.3}
\contentsline {subsubsection}{\numberline {5.8.3.1}gravity}{33}{subsubsection.5.8.3.1}
\contentsline {section}{\numberline {5.9}Spring\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Mass\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Drawable Class Reference}{33}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Detailed Description}{34}{subsection.5.9.1}
\contentsline {section}{\numberline {5.10}Vector2 Class Reference}{34}{section.5.10}
\contentsline {subsection}{\numberline {5.10.1}Constructor \& Destructor Documentation}{35}{subsection.5.10.1}
\contentsline {subsubsection}{\numberline {5.10.1.1}Vector2()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{35}{subsubsection.5.10.1.1}
\contentsline {subsubsection}{\numberline {5.10.1.2}Vector2()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{35}{subsubsection.5.10.1.2}
\contentsline {subsection}{\numberline {5.10.2}Member Function Documentation}{36}{subsection.5.10.2}
\contentsline {subsubsection}{\numberline {5.10.2.1}norm()}{36}{subsubsection.5.10.2.1}
\contentsline {subsubsection}{\numberline {5.10.2.2}norm2()}{36}{subsubsection.5.10.2.2}
\contentsline {subsection}{\numberline {5.10.3}Member Data Documentation}{36}{subsection.5.10.3}
\contentsline {subsubsection}{\numberline {5.10.3.1}x}{36}{subsubsection.5.10.3.1}
\contentsline {subsubsection}{\numberline {5.10.3.2}y}{36}{subsubsection.5.10.3.2}
\contentsline {chapter}{\numberline {6}File Documentation}{37}{chapter.6}
\contentsline {section}{\numberline {6.1}ball.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{37}{section.6.1}
\contentsline {section}{\numberline {6.2}ball.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{37}{section.6.2}
\contentsline {section}{\numberline {6.3}graphics.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{38}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Typedef Documentation}{39}{subsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.1.1}figures\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}t}{39}{subsubsection.6.3.1.1}
\contentsline {subsection}{\numberline {6.3.2}Function Documentation}{39}{subsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.2.1}handle\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Timer()}{39}{subsubsection.6.3.2.1}
\contentsline {subsubsection}{\numberline {6.3.2.2}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D()}{40}{subsubsection.6.3.2.2}
\contentsline {subsubsection}{\numberline {6.3.2.3}run()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{40}{subsubsection.6.3.2.3}
\contentsline {subsubsection}{\numberline {6.3.2.4}run()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{40}{subsubsection.6.3.2.4}
\contentsline {subsection}{\numberline {6.3.3}Variable Documentation}{40}{subsection.6.3.3}
\contentsline {subsubsection}{\numberline {6.3.3.1}figures}{40}{subsubsection.6.3.3.1}
\contentsline {subsubsection}{\numberline {6.3.3.2}running\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Simulation}{40}{subsubsection.6.3.3.2}
\contentsline {subsubsection}{\numberline {6.3.3.3}running\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Simulation\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Time}{40}{subsubsection.6.3.3.3}
\contentsline {subsubsection}{\numberline {6.3.3.4}running\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Simulation\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Time\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Step}{40}{subsubsection.6.3.3.4}
\contentsline {section}{\numberline {6.4}graphics.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{41}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Function Documentation}{41}{subsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.1.1}run()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{41}{subsubsection.6.4.1.1}
\contentsline {subsubsection}{\numberline {6.4.1.2}run()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{42}{subsubsection.6.4.1.2}
\contentsline {section}{\numberline {6.5}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}md File Reference}{42}{section.6.5}
\contentsline {section}{\numberline {6.6}simulation.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{42}{section.6.6}
\contentsline {section}{\numberline {6.7}springmass.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cpp File Reference}{42}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}Function Documentation}{43}{subsection.6.7.1}
\contentsline {subsubsection}{\numberline {6.7.1.1}main()}{43}{subsubsection.6.7.1.1}
\contentsline {subsubsection}{\numberline {6.7.1.2}operator$<$$<$()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{43}{subsubsection.6.7.1.2}
\contentsline {subsubsection}{\numberline {6.7.1.3}operator$<$$<$()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{43}{subsubsection.6.7.1.3}
\contentsline {section}{\numberline {6.8}springmass.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{43}{section.6.8}
\contentsline {subsection}{\numberline {6.8.1}Macro Definition Documentation}{44}{subsection.6.8.1}
\contentsline {subsubsection}{\numberline {6.8.1.1}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}G\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}V\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}TY}{44}{subsubsection.6.8.1.1}
\contentsline {subsubsection}{\numberline {6.8.1.2}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}G\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}V\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}TY}{44}{subsubsection.6.8.1.2}
\contentsline {subsection}{\numberline {6.8.2}Function Documentation}{45}{subsection.6.8.2}
\contentsline {subsubsection}{\numberline {6.8.2.1}dot()}{45}{subsubsection.6.8.2.1}
\contentsline {subsubsection}{\numberline {6.8.2.2}operator$\ast $()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{45}{subsubsection.6.8.2.2}
\contentsline {subsubsection}{\numberline {6.8.2.3}operator$\ast $()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{45}{subsubsection.6.8.2.3}
\contentsline {subsubsection}{\numberline {6.8.2.4}operator+()}{45}{subsubsection.6.8.2.4}
\contentsline {subsubsection}{\numberline {6.8.2.5}operator-\/()}{45}{subsubsection.6.8.2.5}
\contentsline {subsubsection}{\numberline {6.8.2.6}operator/()}{45}{subsubsection.6.8.2.6}
\contentsline {section}{\numberline {6.9}test-\/ball-\/graphics.cpp File Reference}{46}{section.6.9}
\contentsline {subsection}{\numberline {6.9.1}Function Documentation}{46}{subsection.6.9.1}
\contentsline {subsubsection}{\numberline {6.9.1.1}main()}{46}{subsubsection.6.9.1.1}
\contentsline {section}{\numberline {6.10}test-\/ball.cpp File Reference}{47}{section.6.10}
\contentsline {subsection}{\numberline {6.10.1}Function Documentation}{47}{subsection.6.10.1}
\contentsline {subsubsection}{\numberline {6.10.1.1}main()}{47}{subsubsection.6.10.1.1}
\contentsline {subsubsection}{\numberline {6.10.1.2}run()}{47}{subsubsection.6.10.1.2}
\contentsline {section}{\numberline {6.11}test-\/springmass-\/graphics.cpp File Reference}{48}{section.6.11}
\contentsline {subsection}{\numberline {6.11.1}Function Documentation}{48}{subsection.6.11.1}
\contentsline {subsubsection}{\numberline {6.11.1.1}main()}{48}{subsubsection.6.11.1.1}
\contentsline {section}{\numberline {6.12}test-\/springmass.cpp File Reference}{48}{section.6.12}
\contentsline {subsection}{\numberline {6.12.1}Function Documentation}{49}{subsection.6.12.1}
\contentsline {subsubsection}{\numberline {6.12.1.1}main()}{49}{subsubsection.6.12.1.1}
\contentsline {chapter}{Index}{51}{section*.72}
