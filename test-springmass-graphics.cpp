/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
private:
    Figure figure;
public:
    SpringMassDrawable():SpringMass(),figure("Spring Mass"){
        figure.addDrawable(this);
    }
    void draw(int i){
        figure.drawCircle(getMass(i)->getPosition().x, getMass(i)->getPosition().y, getMass(i)->getRadius()); // draw the right one
        figure.drawCircle(getMass(i+1)->getPosition().x, getMass(i+1)->getPosition().y, getMass(i+1)->getRadius()); // draw the left one
        figure.drawLine(getMass(i)->getPosition().x, getMass(i)->getPosition().y, getMass(i+1)->getPosition().x, getMass(i+1)->getPosition().y,0.05);
    }
    void display(){
        figure.update();
    }
};

int main(int argc, char** argv)
{   int i;
    glutInit(&argc,argv);
    SpringMassDrawable springmass;
    const double mass = 0.03 ;
    const double radius = 0.025 ;
    const double naturalLength = 0.95 ;
    const double stiffness = 1;
    const double dt = 1/30.0 ;
    Mass m1(Vector2(-0.5,0), Vector2(), mass, radius);
    Mass m2(Vector2(0.5,0), Vector2(), mass, radius);
    Spring spring(&m1, &m2, naturalLength, stiffness);
    springmass.addMass(&m1);
    springmass.addMass(&m2);
    springmass.addSpring(&spring);
    for(i=0; i < springmass.getMassVectorLengh(); i+=2){
        springmass.display();
        springmass.draw(i);
    }
    run(&springmass, dt);
}