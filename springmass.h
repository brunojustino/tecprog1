/** file: springmass.h
 ** brief: SpringMass simulation
 ** author: Andrea Vedaldi
 **/

#ifndef __springmass__
#define __springmass__

#include "simulation.h"

#include <cmath>
#include <vector>

#define MOON_GRAVITY 1.62
#define EARTH_GRAVITY 9.82

/* ---------------------------------------------------------------- */
// class Vector2
/* ---------------------------------------------------------------- */

class Vector2
{
public:
  double x ;
  double y ;

  Vector2() : x(0), y(0) { }
  Vector2(double _x, double _y) : x(_x), y(_y) { }
  double norm2() const { return x*x + y*y ; }
  double norm() const { return std::sqrt(norm2()) ; }
};

inline Vector2 operator+ (Vector2 a, Vector2 b) { return Vector2(a.x+b.x, a.y+b.y) ; }
inline Vector2 operator- (Vector2 a, Vector2 b) { return Vector2(a.x-b.x, a.y-b.y) ; }
inline Vector2 operator* (double a, Vector2 b)  { return Vector2(a*b.x, a*b.y) ; }
inline Vector2 operator* (Vector2 a, double b)  { return Vector2(a.x*b, a.y*b) ; }
inline Vector2 operator/ (Vector2 a, double b)  { return Vector2(a.x/b, a.y/b) ; }
inline Vector2 operator/ (double b,  Vector2 a)  { return Vector2(b/a.x, b/a.y)  ; }
inline double dot(Vector2 a, Vector2 b) { return a.x*b.x + a.y*b.y ; }

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

class Mass
{
public:
  Mass() ;
  Mass(Vector2 position, Vector2 velocity, double mass, double radius); // test-springmass.cpp calls it
  void setForce(Vector2 f) ;
  void addForce(Vector2 f) ;
  Vector2 getForce() const ;
  Vector2 getPosition() const ;
  Vector2 getVelocity() const ;
  double getMass() const ;
  double getRadius() const ;
  double getEnergy(double gravity) const ;
  void step(double dt,double gravity/*,const Spring& spring*/) ;


protected:
  Vector2 position;
  Vector2 velocity;
  Vector2 force;
  double mass;
  double radius;
  //Box limits
  double xmin;
  double xmax;
  double ymin;
  double ymax;
};

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

class Spring
{
public:
  Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping = 0.01) ;
  Mass * getMass1() const ;
  Mass * getMass2() const ;
  Vector2 getForce() const ;
  double getLength() const ;
  double getEnergy() const ;
protected:
  double naturalLength;
  double stiffness;
  double damping;
  double force;
  Mass *mass1;
  Mass *mass2;
};

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */
typedef std::vector<Mass*> masses;  // Use it for many balls
typedef std::vector<Spring*> springs; // Use it for balls  && springs-1

class SpringMass : public Simulation
{
public:
  SpringMass(double gravity = MOON_GRAVITY);
  void step(double dt);
  void display();
  double getEnergy() const ;
  void addMass(Mass *mass);
  void addSpring(Spring *s);
  Mass *getMass(int i);
  int getMassVectorLengh();
protected:
  Mass *mass1,*mass2;
  Spring *spring;
  masses m;
  springs sp;
  Spring *naturalLength;
  Spring *stiffness;
  double gravity;
  double dt;
  };
#endif /* defined(__springmass__) */